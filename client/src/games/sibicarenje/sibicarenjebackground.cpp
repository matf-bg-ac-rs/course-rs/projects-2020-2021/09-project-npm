#include "headers/games/sibicarenje/sibicarenjebackground.h"

#include <QGraphicsScene>
#include <QPainter>

SibicarenjeBackground::SibicarenjeBackground()
{
    m_image = QImage(":assets/images/games/sibicarenje/sibicarenjeBackground.jpg");
}

QRectF SibicarenjeBackground::boundingRect() const
{
    return QRectF(scene()->sceneRect().x()-5, scene()->sceneRect().y()-5,
                  scene()->sceneRect().width()+10, scene()->sceneRect().height()+10);
}

void SibicarenjeBackground::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawImage(boundingRect(), m_image, m_image.rect());
    Q_UNUSED(option);
    Q_UNUSED(widget);
}
