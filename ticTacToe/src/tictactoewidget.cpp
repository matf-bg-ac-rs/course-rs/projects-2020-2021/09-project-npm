#include "headers/tictactoewidget.h"

#include <QGraphicsItem>
#include <QGridLayout>
#include <QSignalMapper>

TicTacToeWidget::TicTacToeWidget(QWidget *parent)
    : QWidget(parent)
{
    m_currentPlayer = Player::Invalid;
    m_player = Player::Invalid;

    QGridLayout* gridLayout = new QGridLayout(this);
    QSignalMapper* mapper = new QSignalMapper(this);

    for(int i=0; i<3; i++) {
        for(int j=0; j<3; j++) {
            QPushButton* button = new QPushButton(" ");
            button->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
            gridLayout->addWidget(button, i, j);
            m_board.append(button);

            mapper->setMapping(button, m_board.count() - 1);
            connect(button, SIGNAL(clicked()), mapper, SLOT(map()));
        }
    }

    connect(mapper, SIGNAL(mapped(int)), this, SLOT(handleButtonClick(int)));
}

void TicTacToeWidget::clearBoard()
{
    for(QPushButton* button : m_board) {
        button->setText(" ");
    }
}

void TicTacToeWidget::initNewGame()
{
    clearBoard();

    QJsonObject pickGameMsg;
    pickGameMsg[tr("type")] = tr("pickGame");
    pickGameMsg[tr("choice")] = 0; // fiksni kod za iks oks, samo testiranje

    emit sendMove(pickGameMsg);
}

int TicTacToeWidget::gameIndex() const
{
    return m_gameIndex;
}

void TicTacToeWidget::gameIndex(const int gameIndex)
{
    m_gameIndex = gameIndex;
}

void TicTacToeWidget::opponentMove(const int index)
{
    char opponentSymbol = m_player == Player1 ? 'O' : 'X';
    m_board[index]->setText(tr("%1").arg(opponentSymbol));

    const Player winner = checkCondition();
    if(winner == Player::Invalid) {
        currentPlayer(currentPlayer() == Player::Player1 ?
                          Player::Player2 : Player::Player1);
        return;
    } else {
        emit(gameOver(winner));
    }
}

TicTacToeWidget::Player TicTacToeWidget::currentPlayer() const {
    return m_currentPlayer;
}

void TicTacToeWidget::currentPlayer(TicTacToeWidget::Player p) {
    if(m_currentPlayer == p) {
        return;
    }

    m_currentPlayer = p;
}

void TicTacToeWidget::player(int p)
{
    m_player = p == 1 ? Player1: Player2;
}

TicTacToeWidget::Player TicTacToeWidget::checkCondition() const
{
    Player winner = Player::Draw;

    for(int i=0; i<3; i++) {
        if(m_board[i]->text() != " " && m_board[i]->text()==m_board[i+1]->text() && m_board[i]->text()== m_board[i+2]->text()) {
            winner = m_board[i]->text() == "X" ? Player::Player1 : Player::Player2;
            return winner;
        }

        if(m_board[i]->text() != " " && m_board[i]->text()==m_board[i+3]->text() && m_board[i]->text()== m_board[i+6]->text()) {
            winner = m_board[i]->text() == "X" ? Player::Player1 : Player::Player2;
            return winner;
        }
    }

    if(m_board[0]->text() != " " && m_board[0]->text()==m_board[4]->text() && m_board[0]->text()== m_board[8]->text()) {
        winner = m_board[0]->text() == "X" ? Player::Player1 : Player::Player2;
        return winner;
    }

    if(m_board[2]->text() != " " && m_board[2]->text()==m_board[4]->text() && m_board[2]->text()==m_board[6]->text()) {
        winner = m_board[2]->text() == "X" ? Player::Player1 : Player::Player2;
        return winner;
    }


    for(QPushButton* button : m_board) {
        if(button->text() == " ") {
            winner = Player::Invalid;
            break;
        }
    }

    return winner;
}

void TicTacToeWidget::handleButtonClick(int index)
{
    if(m_currentPlayer == Player::Invalid || m_currentPlayer != m_player
            || checkCondition() != Player::Invalid) {
        return;
    }

    if(index < 0 || index >= m_board.size()) {
        return;
    }

    QPushButton* button = m_board[index];
    if(button->text() != " ") {
        return;
    }

    button->setText(currentPlayer() == Player::Player1 ? "X" : "O");

    Player winner = checkCondition();

    QJsonObject gameplayMessage;
    gameplayMessage[tr("type")] = tr("play");
    gameplayMessage[tr("gameIndex")] = m_gameIndex;
    gameplayMessage[tr("move")] = index;
    gameplayMessage[tr("outcome")] = static_cast<int>(winner);

    emit(sendMove(gameplayMessage));

    if(winner == Player::Invalid) {
        currentPlayer(currentPlayer() == Player::Player1 ?
                          Player::Player2 : Player::Player1);
        return;
    } else {
        emit(gameOver(winner));
    }
}
