#ifndef SMASH_ITINSTRUCTIONS_H
#define SMASH_ITINSTRUCTIONS_H

#include <QGraphicsItem>

class Smash_ItInstructions: public QGraphicsItem
{
public:
    Smash_ItInstructions();

    // QGraphicsItem interface
public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

protected:
    // ne zelimo da nam polja bilo sta rade u instructions delu - stoga
    // cemo samo prihvatiti dogadjaj kako se ne bi prosledjivao poljima,
    // a instancu ove klase postaviti na scenu posle polja
    // naravno, neophodno je da ona svojim boundingRect() pokrije sva polja
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;
};

#endif // SMASH_ITINSTRUCTIONS_H
