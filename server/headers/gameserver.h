#ifndef GAMESERVER_H
#define GAMESERVER_H

#include "game.h"
#include "serverworker.h"
#include <QHash>
#include <QJsonObject>
#include <QTcpServer>
#include <memory>

class GameServer : public QTcpServer
{
    Q_OBJECT
    Q_DISABLE_COPY(GameServer)
public:
    explicit GameServer(QObject* parent = nullptr);

    // QTcpServer interface

signals:
    void logMessage(const QString& msg);

public slots:
    void stopServer();

protected:
    void incomingConnection(qintptr handle) override;

private slots:
    void jsonReceived(QJsonObject jsonObj);
    void playerDisconnected(ServerWorker *sender);

private:
    void sendJson(ServerWorker *destination, const QJsonObject &message);
    void gameInit(ServerWorker* player1, ServerWorker* player2);
    void playMoveExchange(QJsonObject& jsonObj);
    void onPickGame(QJsonObject& jsonObj);
    void offerPlayAgain(const int gameIndex);
    void onGameInterrupted(ServerWorker* gonePlayer);
    void tryGameStart(Game::GameId game);

private:
    QList<ServerWorker*> m_clients;
    QHash<int, Game*> m_games;

    QHash<Game::GameId, QList<ServerWorker*>> m_gameQueues;
};

#endif // GAMESERVER_H
