#ifndef TILEPANEL_H
#define TILEPANEL_H

#include <QGraphicsItem>
#include <QObject>
#include <QPainter>

class TilePanel : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    explicit TilePanel(const qreal x, const qreal y, const qreal width, const qreal height,
                       const int level, const int battalions, const int defense);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void setLevel(int level);

    void setBattalions(int battalions);

    void setMobilizedTroops(bool mobilizedTroops);

    void setDefense(int defense);

    int level() const;

    int battalions() const;

    int defense() const;

signals:

private:
    QRectF m_rect;
    int m_level;
    int m_battalions;
    bool m_mobilizedTroops;
    int m_defense;
};

#endif // TILEPANEL_H
