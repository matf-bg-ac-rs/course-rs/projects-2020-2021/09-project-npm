#include "../headers/serverworker.h"

#include <QAbstractSocket>
#include <QDataStream>
#include <QJsonDocument>

ServerWorker::ServerWorker(QObject *parent)
    : QObject(parent)
    , m_serverSocket(new QTcpSocket(this))
{
    connect(m_serverSocket, &QTcpSocket::disconnected, this, &ServerWorker::disconnectFromClient);
    connect(m_serverSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::errorOccurred),
            this, &ServerWorker::error);
    connect(m_serverSocket, &QTcpSocket::readyRead, this, &ServerWorker::onReadyRead);
}

bool ServerWorker::setSocketDescriptor(qintptr socketDescriptor)
{
    return m_serverSocket->setSocketDescriptor(socketDescriptor);
}

void ServerWorker::disconnectFromClient()
{
    m_serverSocket->disconnectFromHost();
    emit disconnectedFromClient();
}

/**
 * @brief ServerWorker::onReadyRead
 *
 * Pokusava da ucita JSON poruku iz primljenih bajtova i u slucaju uspeha
 * salje na dalju obradu.
 */
void ServerWorker::onReadyRead()
{
    QByteArray jsonData;
    QDataStream socketStream(m_serverSocket);
    socketStream.setVersion(QDataStream::Qt_5_12);

    while(true) {
        socketStream.startTransaction();
        socketStream >> jsonData;
        if(socketStream.commitTransaction()) {

            QJsonParseError parseError;
            const QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData, &parseError);
            if(parseError.error == QJsonParseError::NoError && jsonDoc.isObject()) {

                 emit jsonReceived(jsonDoc.object());
            }

        } else {
            break;
        }
    }
}

int ServerWorker::gameChoice() const
{
    return m_gameChoice;
}

void ServerWorker::gameChoice(int gameChoice)
{
    m_gameChoice = gameChoice;
}

void ServerWorker::gameIndex(int gameIndex)
{
    m_gameIndex = gameIndex;
}

int ServerWorker::gameIndex() const
{
    return m_gameIndex;
}

void ServerWorker::sendJson(const QJsonObject &json)
{
    const QByteArray jsonData = QJsonDocument(json).toJson(QJsonDocument::Compact);

    QDataStream socketStream(m_serverSocket);
    socketStream.setVersion(QDataStream::Qt_5_12);

    socketStream << jsonData;
}

