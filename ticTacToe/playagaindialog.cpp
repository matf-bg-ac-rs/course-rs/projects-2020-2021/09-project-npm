#include "playagaindialog.h"
#include "ui_playagaindialog.h"

PlayAgainDialog::PlayAgainDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlayAgainDialog)
{
    ui->setupUi(this);
}

PlayAgainDialog::~PlayAgainDialog()
{
    delete ui;
}
