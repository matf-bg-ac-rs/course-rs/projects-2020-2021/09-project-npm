#ifndef BALL_H
#define BALL_H

#include "headers/games/sibicarenje/currentbox.h"
#include "headers/games/sibicarenje/box.h"

#include <QGraphicsObject>

class Ball: public QGraphicsObject
{
    Q_OBJECT
public:
    Ball(Box*);

public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    CurrentBox currentBox() const;

private:
    QRectF m_boundingRect = QRectF(0, 0, 50, 50);
    // kutija u kojoj se nalazi loptica
    Box* m_box;
    // vektor pomeranja loptice, sluzi zarad lepog pozicioniranja loptice u kutiji
    constexpr static QPointF m_vector = QPointF(165,150);
};

#endif // BALL_H
