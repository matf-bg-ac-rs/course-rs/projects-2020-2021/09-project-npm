#include "../../../headers/games/paras_menuitem_lib/instructionspanel.h"

InstructionsPanel::InstructionsPanel(const qreal x, const qreal y, const qreal width, const qreal height, const QString& text)
    : m_rect(QRectF(x, y, width, height))
    , m_text(text)
{

}

QRectF InstructionsPanel::boundingRect() const
{
    return m_rect;
}

void InstructionsPanel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->fillRect(m_rect, QBrush(Qt::white));
    painter->drawText(m_rect, Qt::AlignCenter, m_text);

    Q_UNUSED(option)
    Q_UNUSED(widget)
}
