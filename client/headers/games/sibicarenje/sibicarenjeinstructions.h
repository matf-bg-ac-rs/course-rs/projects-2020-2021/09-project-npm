#ifndef SIBICARENJEINSTRUCTIONS_H
#define SIBICARENJEINSTRUCTIONS_H

#include <QGraphicsItem>

class SibicarenjeInstructions: public QGraphicsItem
{
public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;;
};

#endif // SIBICARENJEINSTRUCTIONS_H
