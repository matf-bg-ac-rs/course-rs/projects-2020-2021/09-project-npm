#ifndef INSTRUCTIONSPANEL_H
#define INSTRUCTIONSPANEL_H

#include <QGraphicsItem>
#include <QPainter>

class InstructionsPanel : public QGraphicsItem
{
public:
    InstructionsPanel(const qreal x, const qreal y, const qreal width, const qreal height, const QString& text);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    QRectF m_rect;
    QString m_text;

};

#endif // INSTRUCTIONSPANEL_H
