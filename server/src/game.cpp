#include "../headers/game.h"

Game::Game(QObject* parent)
    : QObject(parent)
{
}

void Game::gameId(const GameId &value)
{
    m_gameId = value;
}

void Game::player1(ServerWorker *value)
{
    m_player1 = value;
}

void Game::player2(ServerWorker *value)
{
    m_player2 = value;
}

ServerWorker *Game::player1() const
{
    return m_player1;
}

ServerWorker *Game::player2() const
{
    return m_player2;
}

