#include "headers/games/sibicarenje/notifications.h"
#include "ui_notifications.h"

Notifications::Notifications(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Notifications)
{
    ui->setupUi(this);
    connect(ui->pushButton, &QPushButton::clicked, this, &Notifications::close);
}

Notifications::~Notifications()
{
    delete ui;
}

void Notifications::setMessage(QString msg)
{
    ui->message->setText(msg);
}
