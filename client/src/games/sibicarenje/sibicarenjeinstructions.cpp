#include "headers/games/sibicarenje/sibicarenjeinstructions.h"

#include <QPainter>

QRectF SibicarenjeInstructions::boundingRect() const{
    return QRectF(-150,-120,600,500);
}

void SibicarenjeInstructions::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    QFont font = painter->font() ;
    font.setPointSize(font.pointSize() * 1.5);
    painter->setPen(QPen(QColor(255,255,255)));
    painter->setFont(font);
    painter->drawText(boundingRect(),
                      "There are three boxes in total and a ball in one of them. "
                      "At the start of every level, the box containing that ball will close, and then "
                      "all the boxes will start rotating and changing positions.\n"
                      "The goal of the game is simple - player needs to guess correctly "
                      "in which box is ball hidden.\n"
                      "If guessed correctly, player can play next level, where boxes will rotate even faster.\n"
                      "Player guesses the box by pressing on it (press the box on the left).\n"
                      "There are 15 levels in total, but last ones are insanely hard, so good luck."
                      );

    Q_UNUSED(option);
    Q_UNUSED(widget);
}
