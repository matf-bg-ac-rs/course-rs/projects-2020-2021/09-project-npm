#include "../../../headers/games/paras_menuitem_lib/messagefield.h"

#include <QFont>

MessageField::MessageField(const qreal x, const qreal y,
                           const qreal width, const qreal height,
                           const QString& message, const bool isError)
    : m_rect(QRectF(x, y, width, height))
    , m_message(message)
    , m_isError(isError)
{
    setEnabled(false);
}

QRectF MessageField::boundingRect() const
{
    return m_rect;
}

void MessageField::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPen pen = m_isError ? QPen(Qt::red) : QPen(Qt::black);
    painter->setPen(pen);

    QFont font;
    font.setPixelSize(17);
    painter->setFont(font);

    painter->drawText(m_rect, Qt::AlignCenter, m_message);

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

