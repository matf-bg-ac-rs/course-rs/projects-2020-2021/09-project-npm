#ifndef SERVERWINDOW_H
#define SERVERWINDOW_H

#include "gameserver.h"

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class ServerWindow; }
QT_END_NAMESPACE

class ServerWindow : public QWidget
{
    Q_OBJECT

public:
    ServerWindow(QWidget *parent = nullptr);
    ~ServerWindow();

private slots:
    void toggleStartServer();
    void logMessage(const QString &msg);

private:
    Ui::ServerWindow *ui;
    GameServer* m_gameServer;
};
#endif // SERVERWINDOW_H
